<?php

declare(strict_types=1);

namespace Algorithm\HashTable;

/**
 *
 * Метод цепочек
 */
class HashTable
{
    private $size = 0;
    private $loadFactor = 75;

    /** @var CustomArray $buckets */
    private $buckets;

    public function __construct()
    {
        $this->buckets = new CustomArray(10);
    }

    public function put(string $stringKey, $value)
    {
        if ($this->loadFactor <= $this->getLoadPercent()) {
            $this->rehash();
        }

        $key = new Key($stringKey);
        $node = new Node($key, $value);

        $index = $this->getIndexByHash($key->getHash());

        if (isset($this->buckets[$index])) {
            $node->setNext($this->buckets[$index]);
        }

        $this->buckets[$index] = $node;

        $this->size++;
    }

    public function get(string $stringKey)
    {
        $key = new Key($stringKey);
        $index = $this->getIndexByHash($key->getHash());
        if (!isset($this->buckets[$index])) {
            return null;
        }

        /** @var Node $currentNode */
        $currentNode = $this->buckets[$index];
        while($currentNode !== null) {
            if ($currentNode->getKey()->getValue() === $stringKey) {
                return $currentNode->getValue();
            }
            $currentNode = $currentNode->getNext();
        }

        return null;
    }

    public function delete(string $stringKey): bool
    {
        $key = new Key($stringKey);
        $index = $this->getIndexByHash($key->getHash());

        if (!isset($this->buckets[$index])) {
            return false;
        }

        /** @var Node $currentNode */
        $currentNode = $this->buckets[$index];
        $lastNode = $currentNode;

        $success = false;
        while($currentNode !== null) {
            if ($currentNode->getKey()->getValue() === $stringKey) {
                $lastNode->setNext($currentNode->getNext());

                $this->size--;
                $success = true;
                break;
            }
            $lastNode = $currentNode;
            $currentNode = $currentNode->getNext();
        }

        return $success;
    }

    private function rehash()
    {
        $oldStorage = $this->buckets;

        $newCap = ($oldStorage->getCap() * 2) + 1;
        $this->buckets = new CustomArray($newCap);

        /** @var Node $currentNode */
        foreach($oldStorage as $currentNode) {
            while($currentNode !== null) {
                $index = $currentNode->getKey()->getHash() % $newCap;
                if (!isset($this->buckets[$index])) {
                    $this->buckets[$index] = $currentNode;
                    $currentNode = $currentNode->getNext();

                    continue;
                }

                /** @var Node $nextNode */
                $nextNode = $this->buckets[$index];

                do {
                    $lastNode = $nextNode;
                    $nextNode = $nextNode->getNext();
                } while($nextNode !== null);

                $lastNode->setNext($currentNode);
                $currentNode = $currentNode->getNext();
            }
        }
    }

    private function getLoadPercent(): float
    {
        return $this->size * 100 / $this->buckets->getCap();
    }

    private function getIndexByHash(int $hash): int
    {
        return $hash % $this->buckets->getCap();
    }
}
