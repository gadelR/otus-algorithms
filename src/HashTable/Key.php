<?php

declare(strict_types=1);

namespace Algorithm\HashTable;

class Key
{
    private $value;
    private $hash = 0;

    public function __construct(string $key)
    {
        $this->value = $key;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getHash(): int
    {
        if ($this->hash === 0) {
            $this->calcHash();
        }

        return $this->hash;
    }

    private function calcHash()
    {
        foreach (str_split($this->value) as $char) {
            $this->hash += ord($char);
        }
    }
}