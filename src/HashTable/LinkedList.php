<?php

declare(strict_types=1);

namespace Algorithm\HashTable;

class LinkedList
{
    private $node;

    public function addToBeginning(Node $node)
    {
        if ($this->node !== null) {
            $node->setNext($this->node);
        }
        $this->node = $node;
    }

    public function addToEnd(Node $node)
    {
        if ($this->node !== null) {
            $this->node->setNext($node);
            return;
        }

        $this->node = $node;
    }

    public function getCurrentNode()
    {
        return $this->node;
    }
}
