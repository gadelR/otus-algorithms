<?php

declare(strict_types=1);

namespace Algorithm\HashTable;

class CustomArray implements \ArrayAccess, \Countable
{
    private $size = 0;
    private $cap;
    private $container = [];

    public function __construct(int $cap = 0)
    {
        $this->cap = $cap;
    }

    public function getCap()
    {
        return $this->cap;
    }

    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    public function offsetGet($offset)
    {
        if (isset($this->container[$offset])) {
            return $this->container[$offset];
        }

        return null;
    }

    public function offsetSet($offset, $value)
    {
        if ($this->cap <= $this->size) {
            throw new \Exception("Array is full");
        }

        $new = !isset($this->container[$offset]);
        $this->container[$offset] = $value;
        $new && $this->size++;
    }

    public function offsetUnset($offset)
    {
        if (!isset($this->container[$offset])) {
            return false;
        }

        unset($this->container[$offset]);
        $this->size--;

        return true;
    }

    public function count()
    {
        return count($this->container);
    }
}