<?php

declare(strict_types=1);

namespace Algorithm\HashTable;

class Node
{
    /** @var Key */
    private $key;

    /** @var mixed */
    private $value;

    /** @var Node|null */
    private $next;

    public function __construct(Key $key, $value, ?Node $next = null)
    {
        $this->key = $key;
        $this->value = $value;
        $this->next = $next;
    }

    public function getKey(): Key
    {
        return $this->key;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function getNext(): ?Node
    {
        return $this->next;
    }

    public function setNext(?Node $next)
    {
        $this->next = $next;
    }
}
