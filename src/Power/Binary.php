<?php

declare(strict_types=1);

namespace Algorithm\Power;

class Binary
{
    public function __invoke(float $x, float $y)
    {
        $result = 1;
        for ($i = 0; true; $i++) {
            if ($y < 1) {
                break;
            }

            if ($y % 2 !== 0) {
                $p = $x;
                for ($l = 1; $l <= $i; $l++) {
                    $p = $p * $p;
                }

                $result = $result * $p;
            }

            $y = $y / 2;
        }

        return $result;
    }
}