<?php

declare(strict_types=1);

namespace Algorithm\Power;

class BinaryRecursive
{
    public function __invoke(float $x, float $y)
    {
        return $this->handle($x, $y);
    }

    public function handle(float $x, float $y)
    {
        if ($y < 1) {
            return 1;
        }
        $result = $this->handle($x, $y / 2);

        if ($y % 2 === 0) {
            $result = $result * $result;
        } else {
            $result = $result * $result * $x;
        }

        return $result;
    }
}