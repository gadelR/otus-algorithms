<?php

declare(strict_types=1);

namespace Algorithm\Power;

class Multiplication
{
    public function __invoke(float $x, float $y)
    {
        $result = $x;
        $k = floor(log($y, 2));
        for($i = 1; $i <= $k; $i++) {
            $result = $result * $result;
        }
        for ($i = pow(2, $k); $i < $x; $i++) {
            $result = $result * $y;
        }

        return $result;
    }
}