<?php

namespace Algorithm\Power;

class Iteration
{
    public function __invoke(float $x, float $y)
    {
        $result = 1;
        for($i = 0; $i < $y; $i++) {
            $result = $result * $x;
        }

        return $result;
    }

}