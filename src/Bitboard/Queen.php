<?php

declare(strict_types=1);

namespace Algorithm\Bitboard;

class Queen extends Piece
{
    protected function getMoveMask(int $n)
    {
        $k = 1 << $n;

        $a = $this->sum([
            $k << 7,
            $k << 14,
            $k << 21,
            $k << 28,
            $k << 35,
            $k << 42,
            $k << 49,
            $k << 56
        ], $this->noH);

        $b = $this->sum([
            $k << 9,
            $k << 18,
            $k << 27,
            $k << 36,
            $k << 45,
            $k << 54,
            $k << 63
        ], $this->noA);

        $c = $this->sum([
            $k >> 7,
            $k >> 14,
            $k >> 21,
            $k >> 28,
            $k >> 35,
            $k >> 42,
            $k >> 49,
            $k >> 56
        ], $this->noH);

        $d = $this->sum([
            $k >> 9,
            $k >> 18,
            $k >> 27,
            $k >> 36,
            $k >> 45,
            $k >> 54,
            $k >> 63
        ], $this->noA);


        $e = $this->sum([
            $k >> 1,
            $k >> 2,
            $k >> 3,
            $k >> 4,
            $k >> 5,
            $k >> 6,
            $k >> 7
        ], $this->noH);

        $f = $this->sum([
            $k << 8,
            $k << 16,
            $k << 24,
            $k << 32,
            $k << 40,
            $k << 48,
            $k << 56
        ]);

        $j = $this->sum([
            $k << 1,
            $k << 2,
            $k << 3,
            $k << 4,
            $k << 5,
            $k << 6,
            $k << 7
        ], $this->noA);

        $k = $this->sum([
            $k >> 8,
            $k >> 16,
            $k >> 24,
            $k >> 32,
            $k >> 40,
            $k >> 48,
            $k >> 56
        ]);

        return $a | $b | $c | $d | $e | $f | $j | $k;
    }
}