<?php

declare(strict_types=1);

namespace Algorithm\Bitboard;

class Rook extends Piece
{
    protected function getMoveMask(int $n)
    {
        $k = 1 << $n;

        $a = $this->sum([
            $k >> 1,
            $k >> 2,
            $k >> 3,
            $k >> 4,
            $k >> 5,
            $k >> 6,
            $k >> 7
        ], $this->noH);

        $b = $this->sum([
            $k << 8,
            $k << 16,
            $k << 24,
            $k << 32,
            $k << 40,
            $k << 48,
            $k << 56
        ]);

        $c = $this->sum([
            $k << 1,
            $k << 2,
            $k << 3,
            $k << 4,
            $k << 5,
            $k << 6,
            $k << 7
        ], $this->noA);

        $d = $this->sum([
            $k >> 8,
            $k >> 16,
            $k >> 24,
            $k >> 32,
            $k >> 40,
            $k >> 48,
            $k >> 56
        ]);

        return $a | $b | $c | $d;
    }
}
