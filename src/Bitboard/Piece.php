<?php

declare(strict_types=1);

namespace Algorithm\Bitboard;

abstract class Piece
{
    protected $noH = 0x7f7f7f7f7f7f7f7f;
    protected $noA = 0xFeFeFeFeFeFeFeFe;
    protected $noAB = 0xFcFcFcFcFcFcFcFc;
    protected $noGH = 0x3f3f3f3f3f3f3f3f;

    public function __invoke(int $n)
    {
        $moveMask = $this->getMoveMask($n);

        return (new Popcnt())($moveMask);
    }

    protected function sum(array $moveMasks, float $boundMask = null)
    {
        $r = 0;
        for($i = 0; $i <= count($moveMasks); $i++) {
            if ($boundMask && ($moveMasks[$i] & $boundMask) == 0) {
                break;
            }
            $r |= $moveMasks[$i];
        }

        return $r;
    }

    abstract protected function getMoveMask(int $n);
}