<?php

declare(strict_types=1);

namespace Algorithm\Bitboard;

class Bishop extends Piece
{
    protected function getMoveMask(int $n)
    {
        $k = 1 << $n;

        $a = $this->sum([
            $k << 7,
            $k << 14,
            $k << 21,
            $k << 28,
            $k << 35,
            $k << 42,
            $k << 49,
            $k << 56
        ], $this->noH);

        $b = $this->sum([
            $k << 9,
            $k << 18,
            $k << 27,
            $k << 36,
            $k << 45,
            $k << 54,
            $k << 63
        ], $this->noA);

        $c = $this->sum([
            $k >> 7,
            $k >> 14,
            $k >> 21,
            $k >> 28,
            $k >> 35,
            $k >> 42,
            $k >> 49,
            $k >> 56
        ], $this->noH);

        $d = $this->sum([
            $k >> 9,
            $k >> 18,
            $k >> 27,
            $k >> 36,
            $k >> 45,
            $k >> 54,
            $k >> 63
        ], $this->noA);

        return $a | $b | $c | $d;
    }
}