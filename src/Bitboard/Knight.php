<?php

declare(strict_types=1);

namespace Algorithm\Bitboard;

class Knight extends Piece
{
    protected function getMoveMask(int $n)
    {
        $k = 1 << $n;

        $a = $k << 6;
        $b = $k << 15;
        $c = $k << 17;
        $d = $k << 10;

        $e = $k >> 10;
        $f = $k >> 17;
        $j = $k >> 15;
        $l = $k >> 6;

        return $this->noGH & ($a | $e) |
               $this->noH  & ($b | $f) |
               $this->noA  & ($c | $j) |
               $this->noAB & ($d | $l);
    }
}