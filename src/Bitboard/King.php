<?php

declare(strict_types=1);

namespace Algorithm\Bitboard;

class King extends Piece
{
    protected function getMoveMask(int $n)
    {
        $k = 1 << $n;

        $leftMask = $this->noA & $k;
        $rightMask = $this->noH & $k;

        $a = $leftMask << 7;
        $b = $k << 8;
        $c = $rightMask << 9;
        $d = $leftMask >> 1;

        $e = $rightMask << 1;
        $f = $leftMask >> 9;
        $j = $k >> 8;
        $l = $rightMask >> 7;

        return $a | $b | $c |
               $d |      $e |
               $f | $j | $l;
    }
}