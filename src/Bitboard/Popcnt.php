<?php

declare(strict_types=1);

namespace Algorithm\Bitboard;

class Popcnt
{
    public function __invoke(int $n)
    {
        return $this->c($n);
    }

    private function a(int $n)
    {
        $count = 0;
        while($n > 0) {
            $count++;
            $n &= $n - 1;
        }
        return $count;
    }

    private function b(int $n)
    {
        $temp = [
            0b0000 => 0,
            0b0001 => 1,
            0b0010 => 1,
            0b0011 => 2,
            0b0100 => 1,
            0b0101 => 2,
            0b0110 => 2,
            0b0111 => 3,
            0b1000 => 1,
            0b1001 => 2,
            0b1010 => 2,
            0b1011 => 3,
            0b1100 => 2,
            0b1101 => 3,
            0b1111 => 4,
        ];

        $count = 0;
        while($n > 0) {
            $count += $temp[$n & 15];
            $n >>=4;
        }

        return $count;
    }

    private function c(int $n)
    {
        $count = 0;
        while($n > 0) {
            $count += $n & 1;
            $n >>= 1;
        }

        return $count;
    }
}