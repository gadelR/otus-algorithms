<?php

declare(strict_types=1);

namespace Algorithm\Trees\AVL;

use Algorithm\Trees\BST\Tree as BSTTree;
use Algorithm\Trees\NodeInterface;

class Tree extends BSTTree
{
    public function insert($value): bool
    {
        if ($this->root === null) {
            $this->root = new Node($value);
        } else {
            $this->root->insert(new Node($value));
            $this->root->balance();
            // После балансировки дерева корень может поменяться
            while($this->root->getParent()) {
                $this->root = $this->root->getParent();
            }
        }

        return true;
    }

    public function search($value): bool
    {
        return $this->root->search(new Node($value)) ? true : false;
    }

    public function remove($value): bool
    {
        return $this->root->remove(new Node($value));
    }

    public function print()
    {
        $this->walk($this->root);
    }

    /**
     * @param Node|null $node
     */
    private function walk(?NodeInterface $node)
    {
        if ($node === null) {
            return;
        }
        $this->walk($node->getLeftChild());
        echo $node->getValue();
        $this->walk($node->getRightChild());
    }}