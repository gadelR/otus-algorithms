<?php

declare(strict_types=1);

namespace Algorithm\Trees\AVL;

use Algorithm\Trees\BST\Node as BSTNode;

class Node extends BSTNode
{
    public function balance()
    {
        $bf = $this->getBalanceFactor();

        if ($bf > 1) {
            $leftGrandSon = $this->left ? $this->left->getLeftChild() : null;
            $rightGrandSon = $this->left ? $this->left->getRightChild() : null;

            $leftGrandSonHeight = $leftGrandSon ? $leftGrandSon->getHeight() : 0;
            $rightGrandSonHeight = $rightGrandSon ? $rightGrandSon->getHeight() : 0;

            if ($leftGrandSonHeight < $rightGrandSonHeight) {
                $this->bigRightRotate();
            } else {
                $this->smallRightRotate();
            }
        }
        if ($bf < -1) {
            $leftGrandSon = $this->right ? $this->right->getLeftChild() : null;
            $rightGrandSon = $this->right ? $this->right->getRightChild() : null;

            $leftGrandSonHeight = $leftGrandSon ? $leftGrandSon->getHeight() : 0;
            $rightGrandSonHeight = $rightGrandSon ? $rightGrandSon->getHeight() : 0;

            if ($leftGrandSonHeight > $rightGrandSonHeight) {
                $this->bigLeftRotate();
            } else {
                $this->smallLeftRotate();
            }
        }

        return true;
    }

    public function bigRightRotate()
    {
        $this->left->smallLeftRotate();
        $this->smallRightRotate();
    }

    public function bigLeftRotate()
    {
        $this->left->smallRightRotate();
        $this->smallLeftRotate();
    }

    public function smallLeftRotate()
    {
        $right = $this->right;
        $this->right = $right->getLeftChild();
        $right->setParent($this->parent);
        $this->parent && $this->parent->setRightChild($right);
        $right->insert($this);
    }

    public function smallRightRotate()
    {
        $left = $this->left;
        $this->left = $left->getRightChild();
        $left->setParent($this->parent);
        $this->parent && $this->parent->setLeftChild($left);
        $left->insert($this);
    }
}
