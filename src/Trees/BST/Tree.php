<?php

declare(strict_types=1);

namespace Algorithm\Trees\BST;

use Algorithm\Trees\NodeInterface;
use Algorithm\Trees\TreeInterface;

class Tree implements TreeInterface
{
    protected ?NodeInterface $root = null;

    public function insert($value): bool
    {
        if ($this->root) {
            $this->root->insert(new Node($value));
        } else {
            $this->root = new Node($value);
        }

        return true;
    }

    public function search($value): bool
    {
        return $this->root->search(new Node($value)) ? true : false;
    }

    public function remove($value): bool
    {
        return $this->root->remove(new Node($value));
    }

    public function print()
    {
        $this->walk($this->root);
    }

    /**
     * @param Node|null $node
     */
    private function walk(?NodeInterface $node)
    {
        if ($node === null) {
            return;
        }
        $this->walk($node->getLeftChild());
        echo $node->getValue();
        $this->walk($node->getRightChild());
    }
}