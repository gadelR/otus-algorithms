<?php

declare(strict_types=1);

namespace Algorithm\Trees\BST;

use Algorithm\Trees\NodeInterface;

class Node implements NodeInterface
{
    const LEFT_CHILD_TYPE = 'left';
    const RIGHT_CHILD_TYPE = 'right';

    protected ?NodeInterface $parent = null;
    protected ?NodeInterface $left = null;
    protected ?NodeInterface $right = null;

    protected string $type;
    protected mixed $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function insert(NodeInterface $node): NodeInterface
    {
        if ($this->value === $node->value) {
            throw new \Exception('Node with this value already exist');
        }

        $node->parent = $this;
        if ($this->value > $node->value) {
            $node->type = self::LEFT_CHILD_TYPE;
            $this->left ? $this->left->insert($node) : $this->left = $node;
        }
        if ($this->value < $node->value) {
            $node->type = self::RIGHT_CHILD_TYPE;
            $this->right ? $this->right->insert($node) : $this->right = $node;
        }

        return $node;
    }

    public function search(NodeInterface $node): ?NodeInterface
    {
        if ($this->value > $node->value) {
            if ($this->left === null) {
                return null;
            }
            return $this->left->search($node);
        }
        if ($this->value < $node->value) {
            if ($this->right === null) {
                return null;
            }
            return $this->right->search($node);
        }

        return $this;
    }

    public function remove(NodeInterface $node): bool
    {
        if ($this->value > $node->value) {
            return $this->left !== null && $this->left->remove($node);
        }
        if ($this->value < $node->value) {
            return $this->right !== null && $this->right->remove($node);
        }

        if ($this->left !== null) {
            $this->parent->{$this->type} = $this->left;
            $this->right && $this->left->insert($this->right);
        } elseif ($this->right !== null) {
            $this->parent->{$this->type} = $this->right;
            $this->left && $this->right->insert($this->left);
        } else {
            $this->parent->{$this->type} = null;
        }

        return true;
    }

    public function setParent(?NodeInterface $node): NodeInterface
    {
        $this->parent = $node;

        return $this;
    }

    public function getParent(): ?NodeInterface
    {
        return $this->parent;
    }

    public function setLeftChild(?NodeInterface $node): NodeInterface
    {
        $this->left = $node;

        return $this;
    }

    public function getLeftChild(): ?NodeInterface
    {
        return $this->left;
    }

    public function setRightChild(?NodeInterface $node): NodeInterface
    {
        $this->right = $node;

        return $this;
    }

    public function getRightChild(): ?NodeInterface
    {
        return $this->right;
    }

    public function getValue(): mixed
    {
        return $this->value;
    }

    public function getHeight(): int
    {
        $rightChildHeight = $this->right === null ? 0 : $this->right->getHeight();
        $leftChildHeight = $this->left === null ? 0 : $this->left->getHeight();

        return max([$rightChildHeight, $leftChildHeight]) + 1;
    }

    public function getBalanceFactor(): int
    {
        $rightChildHeight = $this->right === null ? 0 : $this->right->getHeight();
        $leftChildHeight = $this->left === null ? 0 : $this->left->getHeight();

        return $leftChildHeight - $rightChildHeight;
    }
}