<?php

declare(strict_types=1);

namespace Algorithm\Trees;

interface NodeInterface
{
    public function insert(self $node): self;
    public function search(self $node): ?self;
    public function remove(self $node): bool;

    public function setParent(?self $node): self;
    public function getParent(): ?self;

    public function setLeftChild(?self $node): self;
    public function getLeftChild(): ?self;

    public function setRightChild(?self $node): self;
    public function getRightChild(): ?self;

    public function getHeight(): int;

    public function getBalanceFactor(): int;

    public function getValue(): mixed;
}