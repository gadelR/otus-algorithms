<?php

declare(strict_types=1);

namespace Algorithm\Trees;

interface TreeInterface
{
    public function insert($value): bool;
    public function search($value): bool;
    public function remove($value): bool;
}