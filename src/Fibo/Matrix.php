<?php

declare(strict_types=1);

namespace Algorithm\Fibo;

class Matrix
{
    public function __invoke(int $n)
    {
        $m = [
            [1, 1],
            [1, 0]
        ];

        $rm = $this->pow($m, $n - 1);

        return $rm[0][0];
    }

    private function pow(array $m, int $n)
    {
        if ($n <= 2) {
            return $m;
        }

        $r = [];
        for ($i = 0; true; $i++) {
            if ($n < 1) {
                break;
            }
            if ($n % 2 !== 0) {
                $tmp = $m;
                for ($l = 1; $l <= $i; $l++) {
                    $tmp = $this->multMatrix($tmp, $tmp);
                }
                $r = empty($r) ? $tmp : $this->multMatrix($r, $tmp);
            }

            $n = $n / 2;
        }

        return $r;
    }

    private function multMatrix(array $a, array $b)
    {
        $ca = count($a);
        $cb = count($b);
        $p = count($b[0]);

        $r = [];
        for ($i = 0; $i < $ca; $i++) {
            for($j = 0; $j < $p; $j++) {
                $r[$i][$j] = 0;
                for($k = 0; $k < $cb; $k++) {
                    $r[$i][$j] += $a[$i][$k] * $b[$k][$j];
                }
            }
        }

        return $r;
    }
}
