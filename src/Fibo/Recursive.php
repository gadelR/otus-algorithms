<?php

declare(strict_types=1);

namespace Algorithm\Fibo;

class Recursive
{
    public function __invoke(int $n): int
    {
        return $this->calc($n);
    }

    public function calc(int $n)
    {
        if ($n === 0) {
            return 0;
        } elseif ($n < 2) {
            return 1;
        } else {
            return $this->calc($n - 1) + $this->calc($n - 2);
        }
    }
}
