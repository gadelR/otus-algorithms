<?php

declare(strict_types=1);

namespace Algorithm\Fibo;

class Iteration
{
    public function __invoke(int $n)
    {
        if ($n === 0) {
            return 0;
        }
        if ($n < 2) {
            return 1;
        }

        $x = 1;
        $y = 1;
        $r = 1;

        for ($i = 3; $i <= $n; $i++) {
            $r = $x + $y;
            $x = $y;
            $y = $r;
        }

        return $r;
    }
}
