<?php

declare(strict_types=1);

namespace Algorithm\Fibo;

class Golden
{
    public function __invoke(int $n)
    {
        $f = (1 + sqrt(5)) / 2;

        return floor($this->pow($f, $n) / sqrt(5) + 0.5);
    }

    private function pow(float $a, float $n)
    {
        $r = 1;
        for ($i = 0; true; $i++) {
            if ($n < 1) {
                break;
            }
            if ($n % 2 !== 0) {
                $tmp = $a;
                for ($l = 1; $l <= $i; $l++) {
                    $tmp = $tmp * $tmp;
                }
                $r = $tmp * $r;
            }
            $n = $n / 2;
        }

        return $r;
    }
}