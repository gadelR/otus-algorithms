<?php

declare(strict_types=1);

namespace Algorithm\StringLength;

class Checker
{
    public function __invoke($string)
    {
        return mb_strlen($string);
    }
}