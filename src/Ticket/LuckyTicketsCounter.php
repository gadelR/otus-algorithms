<?php

declare(strict_types=1);

namespace Algorithm\Ticket;

class LuckyTicketsCounter
{
    private $n;
    private $counter = 0;

    public function __construct(int $n)
    {
        $this->n = $n;
    }

    public function __invoke()
    {

       return $this->getLuckyCountOne();

        return $this->counter;
    }

    /**
     * Complexity 10^n
     *
     * @param int $k
     * @param int $sumOne
     * @param int $sumTwo
     */
    public function getLuckyCountOne(int $k = 0, int $sumOne = 0, int $sumTwo = 0)
    {
        if ($this->n === $k) {
            if ($sumOne === $sumTwo) {
                $this->counter++;
            }
            return;
        }
        if ($k < ($this->n / 2)) {
            for ($d = 0; $d < 10; $d++) {
                $this->getLuckyCountOne($k + 1, $sumOne + $d, $sumTwo);
            }
        } else {
            for ($d = 0; $d < 10; $d++) {
                $this->getLuckyCountOne($k + 1, $sumOne, $sumTwo + $d);
            }
        }
    }
}