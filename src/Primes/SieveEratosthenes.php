<?php

declare(strict_types=1);

namespace Algorithm\Primes;

class SieveEratosthenes
{
    public function __invoke(int $n)
    {
        $p = array_fill(2, $n - 1, true);
        for ($i = 3; $i <= $n; $i++) {
            if ($i % 2 === 0) {
                continue;
            }
            if ($p[$i] === false) {
                continue;
            }
            for ($k = $i * $i; $k <= $n; $k += $i) {
                $p[$k] = false;
            }
        }

        $count = 0;
        foreach ($p as $item => $isPrime) {
            if ($isPrime) {
                $count++;
            }
        }

        return $count;
    }
}
