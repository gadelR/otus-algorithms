<?php

declare(strict_types=1);

namespace Algorithm\Primes;

class PrimeDivide
{
    private $primes = [];

    public function __invoke(int $n)
    {
        $count = 0;
        for ($i = 2; $i <= $n; $i++) {
            if ($this->isPrime($i)) {
                $this->primes[$i] = $i;
                $count++;
            }
        }

        return $count;
    }

    public function isPrime(int $n)
    {
        if ($n === 2) {
            return true;
        }
        if ($n % 2 === 0) {
            return false;
        }

        $lastPrime = 3;
        foreach ($this->primes as $prime) {
            if ($n % $prime === 0) {
                return false;
            }
            $lastPrime = $prime;
        }

        $sqrt = (int) sqrt($n);
        for ($i = $lastPrime; $i <= $sqrt; $i += 2) {
            if ($n % $i === 0) {
                return false;
            }
        }
        return true;
    }
}