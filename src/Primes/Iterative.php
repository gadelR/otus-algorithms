<?php

declare(strict_types=1);

namespace Algorithm\Primes;

class Iterative
{
    public function __invoke(int $n)
    {
        $count = 0;
        for ($i = 2; $i <= $n; $i++) {
            if ($this->isPrime($i)) {
                $count++;
            }
        }

        return $count;
    }

    public function isPrime(int $n)
    {
        if ($n === 2) {
            return true;
        }
        if ($n % 2 === 0) {
            return false;
        }
        $sqrt = (int) sqrt($n);
        for ($i = 3; $i <= $sqrt; $i += 2) {
            if ($n % $i === 0) {
                return false;
            }
        }
        return true;
    }
}
