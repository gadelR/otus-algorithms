<?php

declare(strict_types=1);

namespace Algorithm\Sort;

/**
 * M  - 1
 * T  - N^2
 * <> - N^2  -> NlogN
 * =  - 3N^2 -> N^2
 * St - +
 * On - +
 * Ad - +
 */
class Insertion
{
    private $debug;

    public function __construct(bool $debug = false)
    {
        $this->debug = $debug;
    }

    public function __invoke(array $input)
    {
        for ($i = 0; $i < count($input); $i++) {
            $j = $i - 1;
            $tmp = $input[$i];
            while($j >= 0 && $input[$j] > $tmp) {
                $input[$j + 1] = $input[$j];
                $j--;
            }
            $input[$j + 1] = $tmp;
        }

        return $input;
    }
}