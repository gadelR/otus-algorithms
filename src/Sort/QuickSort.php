<?php

declare(strict_types=1);

namespace Algorithm\Sort;

/**
 * M  - 1
 * T  - O(NlogN)
 * <> -
 * =  -
 * St - -
 * On - -
 * Ad - - (anti adaptive)
 *
 * Split   - 1
 * Prepare - N
 * Sort    - Recursive
 */
class QuickSort
{
    private $debug;

    private $input = [];

    public function __construct(bool $debug = false)
    {
        $this->debug = $debug;
    }

    public function __invoke(array $input)
    {
        $this->input = $input;
        $this->sort(0, count($this->input) - 1);

        return $this->input;
    }

    private function sort(int $l, int $r)
    {
        if ($l >= $r) {
            return;
        }
        $c = $this->partition($l, $r);
        $this->sort($l, $c - 1);
        $this->sort($c + 1, $r);
    }

    private function partition(int $l, int $r): int
    {
        $pivot = $this->input[$r];
        $a = $l - 1;
        for ($m = $l; $m <= $r; $m++) {
            if ($this->input[$m] <= $pivot) { // to 1 part
                $this->swap(++$a, $m);
            }
        }
        return $a;
    }

    private function swap(int $x, int $y)
    {
        $tmp = $this->input[$x];
        $this->input[$x] = $this->input[$y];
        $this->input[$y] = $tmp;
    }
}