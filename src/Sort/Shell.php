<?php

declare(strict_types=1);

namespace Algorithm\Sort;

/**
 * M  - 1
 * T  - N^~1,5
 * <> - ~
 * =  - ~
 * St - -
 * On - -
 * Ad - -
 */
class Shell
{
    public function __invoke(array $input)
    {
        for ($s = floor(count($input) / 2); $s > 0; $s = floor($s / 2)) {
            for ($i = $s; $i < count($input); $i++) {
                for ($j = $i - $s; $j >= 0 && $input[$j] > $input[$j + $s]; $j -= $s) {
                    $t = $input[$j];
                    $input[$j] = $input[$j + $s];
                    $input[$j + $s] = $t;
                }
            }
        }

        return $input;
    }
}