<?php

declare(strict_types=1);

namespace Algorithm\Sort;

/**
 * M  - 1
 * T  - N^2
 * <> - N^2
 * =  - 3N
 * St - -
 * On - -
 * Ad - -
 */
class Selection
{
    private $input;

    public function __invoke(array $input)
    {
        $this->input = $input;

        for($i = count($this->input) - 1; $i > 0; $i--) {
            $x = 0;
            for($j = 1; $j <= $i; $j++) {
                if ($this->input[$j] > $this->input[$x]) {
                    $x = $j;
                }
            }
            $this->swap($x, $i);
        }

        return $this->input;
    }

    private function swap(int $a, int $b)
    {
        $tmp = $this->input[$a];
        $this->input[$a] = $this->input[$b];
        $this->input[$b] = $tmp;
    }
}