<?php

declare(strict_types=1);

namespace Algorithm\Sort;

/**
 * M  - 1
 * T  - N^2
 * <> - N^2
 * =  - 3N^2
 * St - +
 * On - -
 * Ad - -/+
 */
class Bubble
{
    private $array;

    public function __invoke(array $input)
    {
        $this->array = $input;
        $this->adaptedSort();

        return $this->array;
    }

    private function sort()
    {
        for ($i = 0; $i < count($this->array); $i++) {
            for ($j = 0; $j < count($this->array) - 1; $j++) {
                if ($this->array[$j] > $this->array[$j + 1]) {
                    $this->swap($j, $j + 1);
                }
            }
        }
    }

    private function adaptedSort()
    {
        do {
            $replaced = false;
            for ($i = 0; $i < count($this->array) - 1; $i++) {
                if ($this->array[$i] > $this->array[$i + 1]) {
                    $this->swap($i, $i + 1);
                    $replaced = true;
                }
            }
        } while($replaced);
    }

    private function swap(int $a, int $b)
    {
        $tmp = $this->array[$a];
        $this->array[$a] = $this->array[$b];
        $this->array[$b] = $tmp;
    }
}
