<?php

declare(strict_types=1);

namespace Algorithm\Sort;

/**
 * M  - 1
 * T  - NlogN
 * <> - logN
 * =  - 3N
 * St - -
 * On - -
 * Ad - -
 */
class HeapSort
{
    private $debug;
    private $array;

    public function __construct($debug = false)
    {
        $this->debug = $debug;
    }

    public function __invoke(array $input)
    {
        $this->array = $input;

        for ($i = count($this->array) / 2 - 1; $i >= 0; $i--) {
            $this->heapify($i, count($this->array));
        }
        for ($j = count($this->array) - 1; $j >= 0; $j--) {
            $this->swap(0, $j);
            $this->heapify(0, $j);
        }

        return $this->array;
    }

    private function heapify($root, $size)
    {
        $l = 2 * $root + 1;
        $r = 2 * $root + 2;
        
        $x = $root;
        if ($l < $size && $this->array[$x] < $this->array[$l]) {
            $x = $l;
        }
        if ($r < $size && $this->array[$x] < $this->array[$r]) {
            $x = $r;
        }

        $this->debug && error_log("r - {$root} ({$this->array[$root]}) | x - {$x} ({$this->array[$x]})");
        if ($x === $root) {
            $this->debug && error_log("Skip");
            return;
        }

        $this->debug && error_log('Swap');

        $this->swap($x, $root);
        $this->heapify($x, $size);
    }

    private function swap(int $a, int $b)
    {
        $tmp = $this->array[$a];
        $this->array[$a] = $this->array[$b];
        $this->array[$b] = $tmp;
    }
}
