<?php

declare(strict_types=1);

namespace Algorithm;

class Tester
{
    private $assetsDir;
    private $tester;

    public function __construct(string $assetsDir, callable $tester = null)
    {
        $this->assetsDir = $assetsDir;
        $this->tester = $tester;
    }

    public function test(callable $testable)
    {
        $assets = [];
        foreach (scandir($this->assetsDir) as $fileName) {
            if (preg_match('/(?<name>.*)\.(?<type>(in|out))/', $fileName, $match)) {
                $filePath = "{$this->assetsDir}/{$fileName}";
                $assets[$match['name']][$match['type']] = trim(file_get_contents($filePath));
            }
        }

        foreach ($assets as $testName => $asset) {
            $result = $testable($asset['in']);
            if ($this->check($result, $asset['out'])) {
                echo "(+) {$testName} is successful \n";
            } else {
                echo "(-) {$testName} failed\n";
                echo "\t Input: {$asset['in']}; Result: {$result}; Expected: {$asset['out']}\n";
            }
        }
    }

    private function check($result, $expected)
    {
        if ($this->tester !== null) {
            return ($this->tester)($result, $expected);
        } else {
            return $result == $expected;
        }
    }
}