<?php

use Algorithm\Tester;

require __DIR__ . '/vendor/autoload.php';

$tester = new Tester(__DIR__ . '/assets/Power', function ($result, $expected) {
    return (float) $result === (float) $expected;
});


$tester->test(function ($input) {
    list($x, $y) = explode("\n", $input);
    $power = new Algorithm\Power\Binary();

    return $power((float) $x, (float) $y);
});
